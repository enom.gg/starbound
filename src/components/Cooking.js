import { createStore as createState } from 'solid-js/store'

import Input from './Input'
import Search from './Search'
import food from '--/store/items/food.json'
import prepared from '--/store/items/prepared.json'
import { useKeyed } from '--/store/local'

export const LOCAL_STORAGE_KEY = 'starbound/fridge'

export default function Cooking () {
  const [store, { commit }] = useKeyed(LOCAL_STORAGE_KEY)
  const [state, setState] = createState({ sort: 'recipe', searched: [] })

  const consume = (recipe) => {
    const ingredients = reduce(prepared[recipe])

    Object.keys(ingredients).forEach((ingredient) => {
      commit(ingredient, store[ingredient] - ingredients[ingredient])
    })
  }

  const collapse = () => Object.values(craftable()).every(({ recipe }) => state[recipe])

  const toggle = (name) => {
    if (name === 'all') {
      const visible = collapse()
      Object.values(craftable()).forEach(({ recipe }) => setState(recipe, !visible))
    } else {
      setState(name, (visible) => !visible)
    }
  }

  const craftable = () => {
    const results = {}

    Object.keys(prepared).forEach((recipe) => {
      const ingredients = reduce(prepared[recipe])
      const craftable = Object.keys(ingredients).every((ingredient) => {
        return store[ingredient] >= ingredients[ingredient]
      })

      if (craftable) {
        let quantity

        Object.keys(ingredients).forEach((ingredient) => {
          const max = Math.floor(store[ingredient] / ingredients[ingredient])
          if (quantity === undefined || max < quantity) quantity = max
        })

        results[recipe] = quantity
      }
    })

    return Object.keys(results)
      .map((recipe) => ({ recipe, quantity: results[recipe] }))
      .sort((a, b) => {
        if (state.sort === 'quantity') return Math.sign(b.quantity - a.quantity)
        return Math.sign(a.recipe.localeCompare(b.recipe))
      })
  }

  const reduce = (recipe) => {
    const results = {}

    Object.keys(recipe).forEach((ingredient) => {
      if (prepared[ingredient]) {
        const recursed = reduce(prepared[ingredient])

        Object.keys(recursed).forEach((name) => {
          results[name] = (results[name] ?? 0) + (recipe[ingredient] * recursed[name])
        })
      } else {
        results[ingredient] = (results[ingredient] ?? 0) + recipe[ingredient]
      }
    })

    return results
  }

  return (
    <>
      <div class='grid grid-cols-2'>
        <div class='border-r border-gray-800 p-3'>
          <h1>Cooking</h1>

          <Search items={food.sort()} results={(items) => setState('searched', items)} />

          <div class='grid gap-2 my-2' style='grid-template-columns: repeat(3, min-content) 1fr'>
            <For
              each={state.searched}
              fallback={(
                <p class='col-span-full'>No results found</p>
              )}
            >
              {(name) => (
                <Input name={name} key={LOCAL_STORAGE_KEY} />
              )}
            </For>
          </div>
        </div>

        <div class='p-3'>
          <div
            class='grid gap-2 content-start'
            style='grid-template-columns: 1fr repeat(2, max-content)'
          >
            <h2 class='mb-0'>Recipes</h2>

            <select onChange={(event) => setState('sort', event.target.value)} class='h-9'>
              <option value='recipe'>Name</option>
              <option value='quantity'>Yield</option>
            </select>

            <button onClick={[toggle, 'all']} class='px-3 h-9'>
              {collapse() ? 'Collapse All' : 'Expand All'}
            </button>

            <For
              each={craftable()}
              fallback={(
                <p class='mx-3 col-span-full'>Nothing available</p>
              )}
            >
              {({ recipe, quantity }) => (
                <>
                  <h4 class='ml-3'>
                    {quantity}x <strong class='pl-2'>{recipe}</strong>
                  </h4>

                  <button onClick={[consume, recipe]} class='px-3 h-9'>
                    Consume
                  </button>

                  <button onClick={[toggle, recipe]} class='px-3 h-9'>
                    {state[recipe] ? 'Collapse' : 'Expand'}
                  </button>

                  <Show when={state[recipe]}>
                    <For each={Object.keys(prepared[recipe]).sort()}>
                      {(ingredient) => (
                        <div class='col-span-full grid grid-cols-3 gap-x-3 ml-3'>
                          <div class='text-right'>{ingredient}</div>
                          <div class='col-span-2'>{prepared[recipe][ingredient]}</div>
                        </div>
                      )}
                    </For>
                  </Show>
                </>
              )}
            </For>
          </div>

          <h3 class='border-t border-gray-800 pt-2 my-3'>Selected</h3>

          <div
            class='grid gap-2'
            style='grid-template-columns: repeat(3, min-content) 1fr min-content'
          >
            <For
              each={Object.keys(store).sort()}
              fallback={(
                <p class='mx-3 col-span-full'>Nothing selected</p>
              )}
            >
              {(name) => (
                <>
                  <Input name={name} key={LOCAL_STORAGE_KEY} />

                  <button
                    onClick={() => commit(name, 0, true)}
                    class='w-9 h-9'
                  >
                    x
                  </button>
                </>
              )}
            </For>
          </div>
        </div>
      </div>
    </>
  )
}
