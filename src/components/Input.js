import { useKeyed } from '--/store/local'

export default function Input (props) {
  const [store, { add, remove, update }] = useKeyed(props.key)

  return (
    <>
      <button onClick={[remove, props.name]} class='w-9 h-9'>-</button>

      <input
        onKeyUp={[update, props.name]}
        value={store[props.name] ?? 0}
        class='text-center w-16 h-9'
      />

      <button onClick={[add, props.name]} class='w-9 h-9'>+</button>

      <div>{props.name}</div>
    </>
  )
}
