import { createSignal } from 'solid-js'

import Input from './Input'
import Search from './Search'
import prepared from '--/store/items/prepared.json'
import { useKeyed } from '--/store/local'

export const LOCAL_STORAGE_KEY = 'starbound/recipes'

export default function Recipes () {
  const [store, { commit }] = useKeyed(LOCAL_STORAGE_KEY)
  const [searched, setSearched] = createSignal(Object.keys(prepared).sort())

  const reduce = (recipe) => {
    const food = {}

    Object.keys(recipe).forEach((name) => {
      const quantity = recipe[name]

      if (prepared[name]) {
        reduce(prepared[name]).forEach((item) => {
          food[item.name] = (food[item.name] ?? 0) + (quantity * item.quantity)
        })
      } else {
        food[name] = (food[name] ?? 0) + recipe[name]
      }
    })

    return Object.keys(food).sort().map((name) => ({ name, quantity: food[name] }))
  }

  return (
    <div class='grid grid-cols-2'>
      <div class='p-3 border-r border-gray-800'>
        <h1>Recipes</h1>

        <Search items={Object.keys(prepared).sort()} results={setSearched} />

        <div class='grid gap-2 my-2' style='grid-template-columns: repeat(3, min-content) 1fr'>
          <For
            each={searched()}
            fallback={(
              <p class='col-span-full'>No results found</p>
            )}
          >
            {(name) => (
              <Input name={name} key={LOCAL_STORAGE_KEY} />
            )}
          </For>
        </div>
      </div>

      <div class='p-3'>
        <h2>Ingredients</h2>

        <Show
          when={reduce(store).length}
          fallback={(
            <p class='mx-3'>Nothing selected</p>
          )}
        >
          <dl class='grid gap-x-3 mx-3' style='grid-template-columns: max-content min-content'>
            <For each={reduce(store)}>
              {({ name, quantity }) => (
                <>
                  <dt>{name}</dt>
                  <dd>{quantity}</dd>
                </>
              )}
            </For>
          </dl>
        </Show>

        <h3 class='border-t border-gray-800 pt-2 mt-3'>Selected</h3>

        <div
          class='grid gap-2 my-2'
          style='grid-template-columns: repeat(3, min-content) 1fr min-content'
        >
          <For
            each={Object.keys(store).sort()}
            fallback={(
              <p class='mx-3 col-span-full'>Nothing selected</p>
            )}
          >
            {(name) => (
              <>
                <Input name={name} key={LOCAL_STORAGE_KEY} />

                <button
                  onClick={() => commit(name, 0, true)}
                  class='w-9 h-9'
                >
                  x
                </button>
              </>
            )}
          </For>
        </div>
      </div>
    </div>
  )
}
