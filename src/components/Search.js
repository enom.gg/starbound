import { createComputed } from 'solid-js'
import { createStore as createState } from 'solid-js/store'

export default function Search (props) {
  const [state, setState] = createState({
    search: '',
    value: ''
  })

  const clear = () => {
    setState({ search: '', value: '' })
  }

  const search = (event) => {
    const value = event.target.value.trim()
    setState({ value, search: value.length < 2 ? '' : value })
  }

  createComputed(() => {
    if (state.search) {
      const regex = new RegExp(state.search, 'i')
      props.results(props.items.filter((name) => regex.test(name)))
    } else {
      props.results(props.items)
    }
  })

  return (
    <div class='flex'>
      <input
        onKeyUp={search}
        value={state.value}
        placeholder='Search...'
        class='block flex-1'
      />

      <button
        onClick={clear}
        class='block flex-shrink w-9 h-9 ml-2'
      >
        X
      </button>
    </div>
  )
}
