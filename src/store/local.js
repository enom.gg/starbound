import { createComputed, createContext, useContext } from 'solid-js'
import { createStore as createState } from 'solid-js/store'

const LocalContext = createContext()

export function LocalProvider (props) {
  const [state, setState] = createState()

  const commit = (key, name, quantity, force = false) => {
    const existing = state[key][name] ?? 0
    const total = force ? quantity : existing + quantity

    setState(key, name, total === 0 ? undefined : total)
  }

  const actions = {
    commit,
    add (key, name) {
      commit(key, name, 1)
    },
    init (key, payload = {}) {
      setState(key, payload)
    },
    remove (key, name) {
      commit(key, name, -1)
    },
    update (key, name, event) {
      const int = parseInt(event.target.value, 10)

      commit(key, name, Number.isNaN(int) ? 0 : int, true)
    }
  }

  createComputed(() => {
    Object.keys(state).forEach((key) => save(key, state[key]))
  })

  return (
    <LocalContext.Provider value={[state, actions]}>
      {props.children}
    </LocalContext.Provider>
  )
}

export function load (key, fallback = undefined) {
  try {
    return JSON.parse(window.localStorage.getItem(key)) ?? fallback
  } catch (e) {
    return fallback
  }
}

export function save (key, payload) {
  try {
    window.localStorage.setItem(key, JSON.stringify(payload))
  } catch (e) {
    console.error(key + ' could not save to window.localStorage')
    console.log(e)
  }
}

export function useLocal () {
  return useContext(LocalContext)
}

export function useKeyed (key, fallback = undefined) {
  const [state, { add, commit, init, remove, update }] = useLocal()

  if (state[key] === undefined) init(key, load(key, fallback))

  return [state[key], {
    add: (name) => add(key, name),
    commit: (name, quantity) => commit(key, name, quantity, true),
    remove: (name) => remove(key, name),
    update: (name, event) => update(key, name, event)
  }]
}
