import { createContext, useContext } from 'solid-js'
import { createStore as createState } from 'solid-js/store'

const ItemsContext = createContext()

export function ItemsProvider (props) {
  const [state] = createState()
  const actions = {}

  return (
    <ItemsContext.Provider value={[state, actions]}>
      {props.children}
    </ItemsContext.Provider>
  )
}

export function useItems () {
  return useContext(ItemsContext)
}
