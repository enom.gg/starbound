import { ItemsProvider } from './items'
import { LocalProvider } from './local'

/**
 * Main data provider.
 *
 * @returns {JSX}
 */
export default function Provider (props) {
  return (
    <LocalProvider>
      <ItemsProvider>
        {props.children}
      </ItemsProvider>
    </LocalProvider>
  )
}
