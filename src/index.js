import { render } from 'solid-js/web'
import { Router, hashIntegration } from 'solid-app-router'

import './index.sass'
import App from './components/App'
import Provider from './store'

render(() => (
  <Router source={hashIntegration()}>
    <Provider>
      <App />
    </Provider>
  </Router>
), document.getElementById('root'))
